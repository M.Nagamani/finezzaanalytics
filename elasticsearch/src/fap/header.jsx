import React, { Component } from 'react';
import Dashboard from "./dashboards";
import AddDashbaord from "./addDashboard"
import CustomDashboard from "./customDashboard"
// import Iframe from "./iframe";
import Visulization from "./visulization";
    import FlightControl  from "./addingFlight";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import CustomHeader from "./customHeader"
import Logs from "./addLogs";
import EcommerceAverage from "./addEcomerce";
import EcommerceTotal from "./addEcommerceTotal"
import LogsPlot from './addLogs';
import CustomVisualization from "./customVisulization";
// import saml from "saml2-js";
// import { async } from 'q';
// import {} from "/home/nagamani/KIBANA_HOME/plugins/kibana-time-plugin"
class Header extends Component {
    constructor(props){
        super(props);
        this.state={
            items:[],
            showPopup:false,
            DashboardId:[],
            visulizationList:[],
            showComment:false,
            dashboardPopup:false,
            selectedVisulizationList:[],
            customDashboardPopup:false,
            customDashboardList:[],
            customDashbaord:[],
            customGroupPopup:false,
            customGroupName:"",
          customVisualizationList:[],
          chartOptions:{
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Browser market shares in india August 2019'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Chrome',
                    y: 67.44,
                    sliced: true,
                    selected: true
                }, {
                    name: 'UC Browser',
                    y: 17.81
                }, {
                    name: 'Firefox',
                    y: 2.82
                }, {
                    name: 'KaiOs',
                    y: 2.32
                }, {
                    name: 'Safari',
                    y: 2.24
                }, {
                    name: 'Opera',
                    y: 3.79
                }]
            }]
        
    }

        }
    }
  handleFetching=async (e)=>{
        let newarr=[];
   await fetch("http://localhost:8080/dashboards")
     .then((res)=>res.json())
     .then((data)=>{
        data.forEach((element)=>{
          newarr.push(element)
        })
     })
    this.setState({
        items:newarr
    })
    }
    
    handleOnClick=(e)=>{
       return this.state.items.map((element)=>{
           if(element[1]==e.target.innerHTML)
           {
               let id=element[0].split(':')[1]
this.setState({
    target:e.target.parentNode.parentNode.parentNode,
    showPopup:false
})
 let val=`http://localhost:5601/app/kibana#/dashboard/${id}?embed=true&_g=()`
//  console.log(val);
 e.target.parentNode.parentNode.parentNode.querySelector(".dashboard-name").innerHTML=`Dashboard/${e.target.innerHTML}`
 e.target.parentNode.parentNode.parentNode.querySelector(".iframe").src=val;
//  console.log(e.target.parentNode.parentNode.parentNode.querySelector(".iframe").querySelector(".dshDashboardViewport-withMargins"));
           }
       })
    }
    handleVisulization=async (e)=>{
        let newarr1=[];
        let newarr=[]
    let targetEle=e.target.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.querySelector(".dashboard-name").innerHTML.split('/')[1]
        await  fetch("http://localhost:8080/dashboards")
        .then((res)=>res.json())
        .then((data)=>{
           data.forEach((element)=>{
             newarr1.push(element)
           })
        })
     await  this.setState({
           DashboardId:newarr1
       })
      await  this.state.DashboardId.map(async  (element,index)=>{
          if(element[1]==targetEle){
           await  fetch(`http://localhost:8080/visulizations/${index}`)
        .then(res=>res.json())
        .then((data)=>{
    this.setState({
        visulizationList:data.arr,
        showPopup:true
    })
        })
          }
        })
        // console.log(this.state.visulizationList)
     }
     handleOnClickVisulizations=(e)=>{
        //  console.log(e.target.parentNode.parentNode.querySelector(".input-text").value=(e.target.innerHTML))
     }
     handleCloseButton=(e)=>{
        e.target.parentNode.parentNode.innerHTML="";
     }
     handleComment=(e)=>{
         console.log(e.target.innerHTML)
         let ter=[];
         let newarr=[];
        ter=e.target.parentNode.getElementsByClassName("e-multi-hidden")[0].options;
        //  console.log(ter[0].value,ter[1])
         for(let i=0;i<ter.length;i++){
            newarr.push(ter[i].value);
      
     }
    //  console.log(newarr)
         this.setState({
             selectedVisulizationList:newarr,
             showComment:true
         })
        
    }
     handleAddComment=(e)=>{
         this.setState({
             showComment:false
         })
         
    
        //  }
     }
     handleAddGroup=(e)=>{
         console.log(e.target.innerHTML)
         this.setState({
             customGroupPopup:true
         })
        
     }
     handleSideBar=(e)=>{
       
         this.setState({
             dashboardPopup:true,
             customDashboardPopup:false,
         })
     }
     handleDoubleClick=(e)=>{
         this.setState({
             dashboardPopup:false
         })
     }
     handleCustomDashboard=async (e)=>{
         let newarr1=[]
    //      let newele=[];
    //     await  fetch("http://localhost:8080/dashboards")
    //     .then((res)=>res.json())
    //     .then((data)=>{
    //         this.setState({
    //             customDashbaord:data
    //         })
    //     //    data.forEach((element)=>{
    //     //      newarr1.push(element)
    //     //    })
    //     })
    //    console.log(this.state.customDashbaord)
    //   await  this.state.customDashbaord.map(async  (element,index)=>{
    //        await  fetch(`http://localhost:8080/visulizations/${index}`)
    //     .then(res=>res.json())
    //     .then(async (data)=>{
    //  await data.arr.forEach((list)=>{
    //    newarr1.push(list)
    //   })
       
    //     })
    // //  await   this.setState({customDashboardList:newarr1})
    // })
      await  this.setState({
            //  customDashboardList:newarr1,
             customDashboardPopup:true,
             dashboardPopup:false
         })
    //     //    console.log(this.state.customDashboardList);
   
     }
     handleCustomAddGroup=(e)=>{
       let groupName= e.target.parentNode.querySelector(".custom-text").value;
         let newarr=[];
        let  ter=e.target.parentNode.getElementsByClassName("e-multi-hidden")[0].options;
        console.log(ter)
        for(let i=0;i<ter.length;i++){
            newarr.push(ter[i].value);
      
     }
     console.log(newarr);
       this.setState({
           customGroupName:groupName,
           customVisualizationList:newarr,
      customGroupPopup:true
       })
     }
    render() { 
        return ( 
            <Router>
            <div  className="head">
                <div className="main">
                    <div className="main_dashboard" onClick={(e)=>{this.handleSideBar(e)}}onDoubleClick={(e)=>{this.handleDoubleClick(e)}}>DashBoards</div>
                    <div onClick={(e)=>{this.handleCustomDashboard(e)}}>
                        Custom Dashboard
                    </div>
                 
                </div>
                {/* <div>hello</div> */}
                <AddDashbaord 
                myItems={this.state}
                handleOnClick={this.handleOnClick}
                handleVisulization={this.handleVisulization}
                handleCloseButton={this.handleCloseButton}
                handleAddGroup={this.handleAddGroup}
                handleAddComment={this.handleAddComment}
                handleComment={this.handleComment}
                handleFetching={this.handleFetching}
                    />
                
              {/* <CustomDashboard 
              myInput={this.state}
              handleCustomAddGroup={this.handleCustomAddGroup}
              /> */}
              <CustomHeader 
              myValue={this.state}/>
              {/* <Route path="/customDashboard/[Flights]_Flight_Count_and_Average_Ticket_Price" component={FlightControl}/>
            <Route path="/customDashboard/[Logs]_Unique_Visitors_vs._Average_Bytes" component={LogsPlot}/>
            <Route path="/cusotmDashboard/[eCommerce]_Average_Sales_Price" component={EcommerceAverage}/>
            <Route path="/customDashboard/[eCommerce]Promotion_Tracking" component={EcommerceTotal} />
            <Route path="/customDashboard/customVisualization" component={CustomVisualization}/>
               */}
</div>
</Router>

         
         );
    }
}
 
export default Header;