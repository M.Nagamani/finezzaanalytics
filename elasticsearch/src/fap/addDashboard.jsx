import React, { Component } from 'react';
import Dashboard from "./dashboards";
import Visulization from "./visulization";
function AddDashboard(props){
    if(props.myItems.dashboardPopup==true){
    return (
        <div className="
        grid">
                 <div className="dropdown">
            <button  className="dropbtn" onMouseEnter={(e)=>{props.handleFetching(e)}}>Select Dashoard</button>
           
             <Dashboard 
            myState={props.myItems}
            handleClick={props.handleOnClick}/>
            
          </div>
        
          <div className="dashboard-name">Dashboard/[eCommerce] Revenue Dashboard</div>
          <div className="visulization">
          <button onClick={(e)=>{props.handleVisulization(e)}} className="button">Add Visulization Group</button>
          </div>
          <div>
          <Visulization
          myValue={props.myItems} 
        //   handleVisulizations={props.handleVisulizationList}
        //   handleOnClickVisulizations={props.handleOnClickVisulizations}
          handleCloseButton={props.handleCloseButton}
          handleComment={props.handleComment}
          handleAddComment={props.handleAddComment}
          handleAddGroup={props.handleAddGroup}/>
         </div>
         
          <iframe className="iframe" src="http://localhost:5601/app/kibana#/dashboard/722b74f0-b882-11e8-a6d9-e546fe2bba5f?embed=true&_g=()" width="1080" height="2500"/>
</div>
      );
    }
    else{
        return null;
    }
}


 
export default AddDashboard;