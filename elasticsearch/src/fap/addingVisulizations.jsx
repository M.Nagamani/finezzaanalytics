import React, { Component } from 'react';
   function Adding(props){
    // console.log(props.myInput.visulizationList);
    let itemsList= props.myInput.visulizationList.map((element,index)=>{
        return <div id={index} onClick={(e)=>{props.handleOnClickVisulizations(e)}}>{element}</div>
    })
    return (
        <div className="content-list">
            {itemsList}
        </div>
    )
}
export default Adding;