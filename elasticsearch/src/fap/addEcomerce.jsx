import React, { Component } from 'react';
class EcommerceAverage extends Component {
    state = {  }
    render() { 
        return ( 
            <div className="iframe-container">
                <iframe src="http://localhost:5601/app/kibana#/visualize/edit/4b3ec120-b892-11e8-a6d9-e546fe2bba5f?embed=true&_g=(refreshInterval:(pause:!f,value:900000),time:(from:now-7d,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(vis:(defaultColors:('0+-+50':'rgb(165,0,38)','50+-+75':'rgb(255,255,190)','75+-+100':'rgb(0,104,55)'))),vis:(aggs:!((enabled:!t,id:'1',params:(customLabel:'average+spend',field:taxful_total_price),schema:metric,type:avg)),params:(addLegend:!t,addTooltip:!t,gauge:(alignment:horizontal,backStyle:Full,colorSchema:'Green+to+Red',colorsRange:!((from:0,to:50),(from:50,to:75),(from:75,to:100)),extendRange:!t,gaugeColorMode:Labels,gaugeStyle:Full,gaugeType:Circle,invertColors:!t,labels:(color:black,show:!t),maxAngle:6.283185307179586,minAngle:0,orientation:vertical,percentageMode:!f,scale:(color:%23333,labels:!f,show:!f),style:(bgColor:!f,bgFill:%23eee,bgMask:!f,bgWidth:0.9,fontSize:60,labelColor:!t,mask:!f,maskBars:50,subText:'per+order',width:0.9),type:meter),isDisplayWarning:!f,type:gauge),title:'%5BeCommerce%5D+Average+Sales+Price',type:gauge))" height="350" width="600"></iframe>
            </div>
         );
    }
}
 
export default EcommerceAverage;