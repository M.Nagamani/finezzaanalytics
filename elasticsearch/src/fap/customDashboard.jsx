import React, { Component } from 'react';
import CustomList from "./addCustomList";
import NewGroup from "./newGroup";
// import AddingCustomGroup from "./addingCustomGroup"

function CustomDashboard(props) {
 console.log(props.myInput.customDashboardList)  
if(props.myInput.customDashboardPopup==true){
        return ( 
            <div className="container-Group">
<div className="custom-container">
               
                <span className="custom-button"> Visualizations</span>
              <button className="display-button" onClick={(e)=>{props.handleCustomAddGroup(e)}}>+ Add New Group</button>
              <button className="custom-group">Group name</button>
              <input type="text" className="custom-text" placeholder="enter group name" />
             
             <CustomList 
              myState={props.myInput} 
             />
            
            </div>
            <NewGroup 
            myValue={props.myInput} />
            </div>
         );
    }
    else
    return null;
}
 
export default CustomDashboard;