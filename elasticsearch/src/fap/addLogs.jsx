import React, { Component } from 'react';
class LogsPlot extends Component {
    state = {  }
    render() { 
        return ( 
            <div  className="iframe-container">
                <iframe src="http://localhost:5601/app/kibana#/visualize/edit/e1d0f010-9ee7-11e7-8711-e7a007dcef99?embed=true&_g=(refreshInterval:(pause:!f,value:900000),time:(from:now-7d,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(vis:(colors:('Avg.+Bytes':%2370DBED,'Unique+Visitors':%230A437C))),vis:(aggs:!((enabled:!t,id:'1',params:(customLabel:'Avg.+Bytes',field:bytes),schema:metric,type:avg),(enabled:!t,id:'2',params:(customLabel:'Unique+Visitors',field:clientip),schema:metric,type:cardinality),(enabled:!t,id:'3',params:(drop_partials:!f,extended_bounds:(),field:timestamp,interval:auto,min_doc_count:1,useNormalizedEsInterval:!t),schema:segment,type:date_histogram),(enabled:!t,id:'4',params:(),schema:radius,type:count)),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,categoryAxes:!((id:CategoryAxis-1,labels:(show:!t,truncate:100),position:bottom,scale:(type:linear),show:!t,style:(),title:(),type:category)),grid:(categoryLines:!f,style:(color:%23eee)),legendPosition:right,radiusRatio:17,seriesParams:!((data:(id:'1',label:'Avg.+Bytes'),drawLinesBetweenPoints:!t,interpolate:linear,mode:stacked,show:true,showCircles:!t,type:histogram,valueAxis:ValueAxis-1),(data:(id:'2',label:'Unique+Visitors'),drawLinesBetweenPoints:!f,interpolate:linear,mode:stacked,show:!t,showCircles:!t,type:line,valueAxis:ValueAxis-2)),times:!(),type:area,valueAxes:!((id:ValueAxis-1,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:LeftAxis-1,position:left,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Avg.+Bytes'),type:value),(id:ValueAxis-2,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:RightAxis-1,position:right,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Unique+Visitors'),type:value))),title:'%5BLogs%5D+Unique+Visitors+vs.+Average+Bytes',type:area))" height="350" width="600"></iframe>
            </div>
         );
    }
}
 
export default LogsPlot;