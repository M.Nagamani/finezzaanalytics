import React, { Component } from 'react';
class FlightControl extends Component {
    state = {  }
    render() { 
        return ( 
            <div  className="iframe-container">
               <iframe src="http://localhost:5601/app/kibana#/visualize/edit/c8fc3d30-4c87-11e8-b3d7-01146121b73d?embed=true&_g=(refreshInterval:(pause:!f,value:900000),time:(from:'2019-09-10T18:19:34.283Z',to:'2019-09-10T18:38:06.309Z'))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(vis:(colors:('Average+Ticket+Price':%23629E51,'Flight+Count':%23AEA2E0),legendOpen:!t)),vis:(aggs:!((enabled:!t,id:'3',params:(drop_partials:!f,extended_bounds:(),field:timestamp,interval:auto,min_doc_count:1,useNormalizedEsInterval:!t),schema:segment,type:date_histogram),(enabled:!t,id:'5',params:(customLabel:'Flight+Count'),schema:metric,type:count),(enabled:!t,id:'4',params:(customLabel:'Average+Ticket+Price',field:AvgTicketPrice),schema:metric,type:avg),(enabled:!t,id:'2',params:(field:AvgTicketPrice),schema:radius,type:avg)),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,categoryAxes:!((id:CategoryAxis-1,labels:(show:!t,truncate:100),position:bottom,scale:(type:linear),show:!t,style:(),title:(),type:category)),grid:(categoryLines:!f,style:(color:%23eee)),legendPosition:right,radiusRatio:13,seriesParams:!((data:(id:'5',label:'Flight+Count'),drawLinesBetweenPoints:!t,interpolate:linear,lineWidth:2,mode:stacked,show:!t,showCircles:!f,type:area,valueAxis:ValueAxis-2),(data:(id:'4',label:'Average+Ticket+Price'),drawLinesBetweenPoints:!f,interpolate:linear,lineWidth:2,mode:stacked,show:!t,showCircles:!t,type:line,valueAxis:ValueAxis-1)),times:!(),type:area,valueAxes:!((id:ValueAxis-1,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:LeftAxis-1,position:left,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Average+Ticket+Price'),type:value),(id:ValueAxis-2,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:RightAxis-1,position:right,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Flight+Count'),type:value))),title:'%5BFlights%5D+Flight+Count+and+Average+Ticket+Price',type:area))" height="350" width="600"></iframe>
            </div>
         );
    }
}
 
export default FlightControl;