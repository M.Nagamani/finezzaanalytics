import React, { Component } from 'react';
import { MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
// import Adding from "./addingVisulizations";
import Comment from "./comment";
import NewGroup from "./newGroup";
 function Visulization(props){
     console.log(props.myValue)
if(props.myValue.showPopup==true){
return (
    <div className="list">
        <div>
           <div className="items-list"> 
           <button
           className="visulization-btn">NewGroup</button>
        <input type="text" className="group-name" placeholder="enter group name" />
        <span id='sample'>
   
        <MultiSelectComponent id="mtselement" dataSource={props.myValue.visulizationList} placeholder="select visulizations" />
        </span>

       <button className="close" onClick={(e)=>props.handleCloseButton(e)}>close</button> 
       <button className="comment" onClick={(e)=>props.handleComment(e)}>Add Commentary</button>
       <Comment 
       myData={props.myValue}
       handleAddComment={props.handleAddComment}
  />
       {/* <img src="./download .png"/> */}
       <button className="another-group" onClick={(e)=>props.handleAddGroup(e)}>Add Group</button>
               </div> 
               <NewGroup
               myItems={props.myValue}/>
        </div>
       
    </div>
)
    }
    else return null;
} 
export default Visulization;