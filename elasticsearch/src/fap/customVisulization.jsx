import React, { Component } from 'react';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official' ;
class CustomVisualization extends Component {
    constructor(props){
        super(props)
        this.state={
            chartOptions:{
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Browser market shares in india August 2019'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: [{
                            name: 'Chrome',
                            y: 67.44,
                            sliced: true,
                            selected: true
                        }, {
                            name: 'UC Browser',
                            y: 17.81
                        }, {
                            name: 'Firefox',
                            y: 2.82
                        }, {
                            name: 'KaiOs',
                            y: 2.32
                        }, {
                            name: 'Safari',
                            y: 2.24
                        }, {
                            name: 'Opera',
                            y: 3.79
                        }]
                    }]
                
            }
        }
    }
    render() { 
        return ( 
            <div>  
                <HighchartsReact highcharts={Highcharts} options={this.state.chartOptions}/>
                </div>
         );
    }
}
 
export default CustomVisualization;