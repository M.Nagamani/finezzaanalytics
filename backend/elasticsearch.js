const { Client } = require("@elastic/elasticsearch");
const express = require("express");
const app = express();
const winston = require("winston");
const path = require("path");
let cors = require("cors");
const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "error",
      filename: path.join(__dirname, "/logs/error.log")
    })
  ]
});
app.use(cors());8
let client;
let ELASTIC_USERNAME="admin"
let ELASTIC_PASSWORD="admin";
process.env["NODE_TLS_REJECT_UNAUTHORIZED"]=0;
if (ELASTIC_USERNAME !== undefined) {

  client = new Client({
    node:
      "https://"+ELASTIC_USERNAME+":"+ELASTIC_PASSWORD+"@localhost:9200",
      // "https://localhost:9200",
      headers:{
        // headers,
      //  "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded" 
      }
      
  });
   console.log(client);
}
// } else {
//   client = new Client({ node: "https://localhost:9200" });
//   // console.log(client)
// }
let dashboardsArray;
const dashboardTitleList = async () => {
  try {
    logger.log("error", "Hey, there is an error");
    let searchResults = await client.search({
        index: ".kibana*admintenant",
        // query:{
        //   match:{
        //     type:"dashboard"
        //   }
        // },
        q:"type:dashboard",
        size:400
      
    });
    // console.log("not working")
    console.log(searchResults.body.hits);
    if (searchResults.body.hits.total === 0) {
      throw "Dashboards not available or cannot be fetched";
    }
    dashboardsArray = searchResults.body.hits.hits;
    let dashboardsTitleArray = dashboardsArray.map(el => [
      el._id,
      el._source.dashboard.title
    ]);
    return dashboardsTitleArray;
  } catch (err) {
    return "Error1: " + err;
  }
};
const visulizationsList = async (id = 0) => {
  try {
    let gettingVisulizations = await client.search({
      index: ".kibana*admintenant",
      q: "type:visualization",
      size: 200
    });
    if (
      dashboardsArray !== undefined &&
      gettingVisulizations.body.hits.total !== 0
    ) {
      let visualizationsList = dashboardsArray[id]._source.references;
      let visualizationsIdList = visualizationsList.map(el => el.id);
      let allVisualizationsArray = gettingVisulizations.body.hits.hits;
      console.log("visulizationId", visualizationsIdList);
      let visualizationsArray = allVisualizationsArray.filter(el =>
        visualizationsIdList.includes(el._id.split(":")[1])
      );
     let visualizationsTitleArray = visualizationsArray.map(
        el => el._source.visualization.title
      );
      return {
        title: dashboardsArray[id]._source.dashboard.title,
        arr: visualizationsTitleArray
      };
    }
   throw "Either Dashboard not selected or unable to get visualizations list";
  } catch (err) {
    return "Error:" + err;
  }
};
app.get("/", (req, res) => res.send("Hello World!"));
app.get("/dashboards", (req, res) =>
  dashboardTitleList()
    .then(json => {
      if (json instanceof String) {
        return res.status(500).send(json);
      }
      return res.send(json);
    })
    .catch(err => {
      return res.status(500).send(err);
    })
);
app.get("/visulizations/:id", (req, res) => {
  visulizationsList(req.params.id)
    .then(json => {
      if (json instanceof String) {
        return res.status(500).send(json);
      }
      return res.send(json);
    })
    .catch(err => {
      return res.status(500).send(err);
    });
});
// app.post('/api/protected',(req,res)=>{
//   return res.json("welocm")
// })
app.listen(8080, () => console.log("working"));